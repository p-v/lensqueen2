
<!DOCTYPE html>
<html lang="en">
     <head>
     <title>Lensqueen</title>
     <meta charset="utf-8">
     <meta name="format-detection" content="telephone=no" />
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
     <script src="js/jquery.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/script.js"></script>
     <script src="js/superfish.js"></script>
     <script src="js/jquery.equalheights.js"></script>
     <script src="js/jquery.easing.1.3.js"></script>
     <script>

    function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}

     </script>


     </head>
     <body class="" id="top">
<!--==============================header=================================-->
<header>
  <div class="container">
    <div class="grid">
            <h1><a href="index.html"><img src="images/lens.jpg" alt="LensQueen: Online Photography System" width="400" height="210"></a></h1>

    </div>
  </div>
  <div class="clear"></div>
  <div class="menu_block">
    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
      <ul class="sf-menu">
       <li class="current"><a href="index.html">Home</a></li>
       <li><a href="2daypick.html">Today's Pick</a></li>
       <li><a href="contestarchive.php">Contest Archive</a></li>
       <li><a href="lensqueentips.php">Lensqueen Tips</a></li>
       <li><a href="exprtcorner.php">Expert Corner</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="aboutus.php">About Us</a></li>
	   <li><a href="adminregistration.php">registered users</a></li>
	   <li><a href="adminarchive.php">Success Stories</a></li>
	   <li><a href=".php">Theme</a></li>
     </ul>
    </nav>
    <div class="clear"></div>
  </div>
</header>

<!--=====================Content======================-->
<!--section class="content"><div class="ic"></div>
  <div class="container_12">
    <div class="grid_4">
      <h4>section 1</h4>
      <img src="images/page2_img1.jpg" alt="" class="img_inner">
      sec 2    </div>
    <div class="grid_4">
      <h4>sec 3</h4>
      <img src="images/page2_img2.jpg" alt="" class="img_inner">
      sec 4
    </div>
    <div class="grid_4">
      <h4>sec 5</h4>
      <img src="images/page2_img3.jpg" alt="" class="img_inner">
      sec 6<br>
    </div>
    <div class="clear"></div>
  </div>

<--/section>
<!--==============================Bot_block=================================-->
<!--=====================archieve block======================-->
<!section class="content"><div class="ic"></div>
  <div class="container_12">



  </div>

</section>
<!--==============================archieve block=================================-->
<!--==============================footer=================================-->
<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_12">
            <a href="#" onClick="goToByScroll('top'); return false;" class="top"></a><br> LensQueen

          <div class="copy"><span class="brand">LensQueen: Online Photography System </span>  &copy; <span id="copyright-year"></span>  | <a href="#"></a>
          </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>
<script type="text/javascript" src="js/jquerypp.custom.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript">

      $( '#carousel' ).elastislide( {
        minItems : 8
      } );

    </script>
</body>
</html>
