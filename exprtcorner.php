<!DOCTYPE html>
<html lang="en">
     <head>
     <title>Lensqueen</title>
     <meta charset="utf-8">
     <meta name="format-detection" content="telephone=no" />
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
     <script src="js/jquery.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/script.js"></script>
     <script src="js/superfish.js"></script>
     <script src="js/jquery.equalheights.js"></script>
     <script src="js/jquery.easing.1.3.js"></script>
     <script>

    function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}

     </script>


     </head>
     <body class="" id="top">
<!--==============================header=================================-->
<header>
  <div class="container">
    <div class="grid">
            <h1><a href="index.html"><img src="images/lens.jpg" alt="LensQueen: Online Photography System" width="400" height="210"></a></h1>

    </div>
  </div>
  <div class="clear"></div>
  <div class="menu_block">
    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
      <ul class="sf-menu">
       <li class="current"><a href="index.html">Home</a></li>
       <li><a href="2daypick.html">Today's Pick</a></li>
       <li><a href="contestarchive.php">Contest Archive</a></li>
       <li><a href="lensqueentips.php">Lensqueen Tips</a></li>
       <li><a href="exprtcorner.php">Expert Corner</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="aboutus.php">About Us</a></li>
     </ul>
    </nav>
    <div class="clear"></div>
  </div>
</header>

<!--=====================Content======================-->
<!--section class="content"><div class="ic"></div>
  <div class="container_12">
    <div class="grid_4">
      <h4>section 1</h4>
      <img src="images/page2_img1.jpg" alt="" class="img_inner">
      sec 2    </div>
    <div class="grid_4">
      <h4>sec 3</h4>
      <img src="images/page2_img2.jpg" alt="" class="img_inner">
      sec 4
    </div>
    <div class="grid_4">
      <h4>sec 5</h4>
      <img src="images/page2_img3.jpg" alt="" class="img_inner">
      sec 6<br>
    </div>
    <div class="clear"></div>
  </div>

<--/section>
<!--==============================Bot_block=================================-->
<!--=====================archieve block======================-->
<!section class="content"><div class="ic"></div>
  <div class="container_12">

  <!-- Expert corner -->
        <div class="about-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 about section-description wow fadeIn">
	                    <h2>Expert Corner</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
                      <h3>Photography Tips</h3>
	                    <p>
                        Whether you are a beginner or more experienced with photography, there are some tips that will benefit you and give you better results.
                        Here are some common issues that you may have to deal with and some tips on how you can use them to your advantage.
	                    </p>
	                </div>
	            </div>
	        </div>
        </div>

        <!-- Block 1 (tip1) -->
        <div class="block-2-container section-container about-block-2-container">
	        <div class="container">
	            <div class="row">

	            	<div class="col-sm-20 block-2-box block-2-right wow fadeInUp">
	            		<h4>1.Compose in Thirds<h4>
	            		<p>
	            			To use the rule of thirds, imagine four lines, two lying horizontally across the image and two vertical creating nine even squares. Some images will look best with the focal point in the center square, but placing the subject off center will often create a more aesthetically composed photograph. When a photograph is composed using the rule of thirds the eyes will wander the frame. A picture composed by the rule of thirds is more interesting and pleasing to the eye.</p>
	            	</div>
                <div class="col-sm-11 block-2-box block-2-left block-2-media wow fadeInLeft">
	            		<div class="block-2-img-container">
	            			<img src="assets/img/about/tip1.jpg" alt="" data-at2x="assets/img/about/tip1.jpg">
	            			<div class="img-container-line line-1"></div>
	            			<div class="img-container-line line-2"></div>
	            			<div class="img-container-line line-3"></div>
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>

        <!-- Block 2 (tip2) -->
        <div class="block-2-container section-container section-container-gray about-block-2-container">
	        <div class="container">
	            <div class="row">
	            	<div class="col-sm-20 block-2-box block-2-left wow fadeInLeft">
	            		<h4>2. Avoid camera shake</h4>
	            		<p>
                    Camera shake or blur is something that can plague any photographer and here are some ways to avoid it. First, you need to learn how to hold your camera properly; use both hands, one around the body and one around the lens and hold the camera close to your body for support. Also make sure you are using a shutter speed that matches the lens focal length. So if you’re using a 100mm lens, then your shutter speed should be no lower than 1/100th of a second. Use a tripod or monopod whenever possible. In lieu of this, use a tree or a wall to stabilize the camera.
	            		</p>
	            	</div>
	            	<div class="col-sm-11 block-2-box block-2-right block-2-media wow fadeInUp">
	            		<div class="block-2-img-container">
	            			<img src="assets/img/about/tip2.jpg" alt="" data-at2x="assets/img/about/tip2.jpg">
	            			<div class="img-container-line line-1"></div>
	            			<div class="img-container-line line-2"></div>
	            			<div class="img-container-line line-3"></div>
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>

        <!-- Block 3 (tip3) -->
        <div class="block-2-container section-container about-block-2-container">
	        <div class="container">
	            <div class="row">
	            	<div class="col-sm-20 block-2-box block-2-right wow fadeInUp">
	            		<h4>3. The sunny 16 rule<h4>
	            		<p>
                    The idea with the Sunny 16 rule is that we can use it to predict how to meter our camera on a sunny outdoor day. So when in that situation, choose an aperture of f/16 and 1/100th of a second shutter speed (provided you are using ISO 100). You should have a sharp image that is neither under or over exposed. This rule is useful if you don’t have a functioning light meter or if your camera doesn’t have an LCD screen to review the image.
	            		</p>
	            	</div>
                <div class="col-sm-15 block-2-box block-2-left block-2-media wow fadeInLeft">
	            		<div class="block-2-img-container">
	            			<img src="assets/img/about/tip3.jpg" alt="" data-at2x="assets/img/about/tip3.jpg">
	            			<div class="img-container-line line-1"></div>
	            			<div class="img-container-line line-2"></div>
	            			<div class="img-container-line line-3"></div>
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>


        <!-- Block 4 (tip4) -->
        <div class="block-2-container section-container about-block-2-container">
          <div class="container">
              <div class="row">

                <div class="col-sm-20 block-2-box block-2-right wow fadeInUp">
                  <h4>4. Use a Polarizing Filter<h4>
                  <p>
                    If you can only buy one filter for your lens, make it a polarizer. This filter helps reduce reflections from water as well as metal and glass; it improves the colors of the sky and foliage, and it will protect your lens too. There’s no reason why you can’t leave it on for all of your photography. The recommended kind of polarizer is circular because these allow your camera to use TTL (through the lens) metering (i.e. Auto exposure).
                  </p>
                  </div>
                <div class="col-sm-11 block-2-box block-2-left block-2-media wow fadeInLeft">
                  <div class="block-2-img-container">
                    <img src="assets/img/about/tip1.jpg" alt="" data-at2x="assets/img/about/tip1.jpg">
                    <div class="img-container-line line-1"></div>
                    <div class="img-container-line line-2"></div>
                    <div class="img-container-line line-3"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>

        <!-- Block 5 (tip5) -->
        <div class="block-2-container section-container section-container-gray about-block-2-container">
          <div class="container">
              <div class="row">
                <div class="col-sm-20 block-2-box block-2-left wow fadeInLeft">
                  <h4>5. Create a sense of Depth</h4>
                  <p>
                    When photographing landscapes it really helps to create a sense of depth, in other words, make the viewer feel like they are there. Use a wide-angle lens for a panoramic view and a small aperture of f/16 or smaller to keep the foreground and background sharp. Placing an object or person in the foreground helps give a sense of scale and emphasizes how far away the distance is. Use a tripod if possible, as a small aperture usually requires a slower shutter speed.
                  </p>
                </div>
                <div class="col-sm-11 block-2-box block-2-right block-2-media wow fadeInUp">
                  <div class="block-2-img-container">
                    <img src="assets/img/about/tip5.jpg" alt="" data-at2x="assets/img/about/tip5.jpg">
                    <div class="img-container-line line-1"></div>
                    <div class="img-container-line line-2"></div>
                    <div class="img-container-line line-3"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>


  </div>

</section>
<!--==============================archieve block=================================-->
<!--==============================footer=================================-->
<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_12">
            <a href="#" onClick="goToByScroll('top'); return false;" class="top"></a><br> LensQueen

          <div class="copy"><span class="brand">LensQueen: Online Photography System </span>  &copy; <span id="copyright-year"></span>  | <a href="#"></a>
          </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>
<script type="text/javascript" src="js/jquerypp.custom.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript">

      $( '#carousel' ).elastislide( {
        minItems : 8
      } );

    </script>
</body>
</html>
