<?php
	include 'myconnect.php';
  session_start("session2");
  if($_SERVER['REQUEST_METHOD'] == 'GET') {
    $sql = "select z.photo_id, z.user_id, max(total_votes) win_vote, z.th_title, r.* from (select p.photo_id, count(*) total_votes, v.user_id, t.th_title from photo p inner join theme t on p.theme_id = t.th_id inner join vote_user_photo v on p.photo_id = v.photo_id group by photo_id) z inner join registration r on r.ssn = z.user_id";
    $query = mysqli_query($conn, $sql);
    $data = array();
		if($query){
      while($row = mysqli_fetch_array($query)){
        $data[] = array('winner' => $row['name'].' '.$row['lname'], 'votes' => $row['win_vote'], 'title' => $row['th_title']);
      }
    } else {
      echo mysql_error();
    }
    header('Content-Type: application/json');
    echo json_encode($data);
  }
?>
