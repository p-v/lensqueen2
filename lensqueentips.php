<!DOCTYPE html>
<html lang="en">
     <head>
     <title>Lensqueen</title>
     <meta charset="utf-8">
     <meta name="format-detection" content="telephone=no" />
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
     <script src="js/jquery.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/script.js"></script>
     <script src="js/superfish.js"></script>
     <script src="js/jquery.equalheights.js"></script>
     <script src="js/jquery.easing.1.3.js"></script>
     <script>

    function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}

     </script>


     </head>
     <body class="" id="top">
<!--==============================header=================================-->
<header>
  <div class="container">
    <div class="grid">
            <h1><a href="index.html"><img src="images/lens.jpg" alt="LensQueen: Online Photography System" width="400" height="210"></a></h1>

    </div>
  </div>
  <div class="clear"></div>
  <div class="menu_block">
    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
      <ul class="sf-menu">
       <li class="current"><a href="index.html">Home</a></li>
       <li><a href="2daypick.html">Today's Pick</a></li>
       <li><a href="contestarchive.php">Contest Archive</a></li>
       <li><a href="lensqueentips.php">Lensqueen Tips</a></li>
       <li><a href="exprtcorner.php">Expert Corner</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="aboutus.php">About Us</a></li>
     </ul>
    </nav>
    <div class="clear"></div>
  </div>
</header>

<!--=====================Content======================-->
<!--section class="content"><div class="ic"></div>
  <div class="container_12">
    <div class="grid_4">
      <h4>section 1</h4>
      <img src="images/page2_img1.jpg" alt="" class="img_inner">
      sec 2    </div>
    <div class="grid_4">
      <h4>sec 3</h4>
      <img src="images/page2_img2.jpg" alt="" class="img_inner">
      sec 4
    </div>
    <div class="grid_4">
      <h4>sec 5</h4>
      <img src="images/page2_img3.jpg" alt="" class="img_inner">
      sec 6<br>
    </div>
    <div class="clear"></div>
  </div>

<--/section>
<!--==============================Bot_block=================================-->
<!--=====================archieve block======================-->
<!section class="content"><div class="ic"></div>
  <div class="container_12">


  <!-- Lensqueen tips -->
  <div class="lensqueen-tips-container section-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 about section-description wow fadeIn">
                <h2>LENSQUEEN TIPS</h2>
                <div class="divider-1 wow fadeInUp"><span></span></div>
                <h3>Rules and Regulations to follow for the Competition</h3>
                <br>
<p>1. ELIGIBILITY</p>
<li>The competition is open to all Indian Citizens and Permanent Residents residing in India.</li>
<li>Submitted images should be in JPEG format and at least 1,600 pixels wide for a horizontal image or 1,600 pixels tall for a vertical image.</li>
<li>Each participant can submit up to three (3) images.</li>
<li>Digital manipulation that distorts the reality of the images will not be allowed. Basic enhancements such as sharpening, contrast adjustment, or simple cropping will be allowed.</li>
<li>The organiser reserves the right to reject images with incomplete details required under the photo submission page. The submission deadline will be declared on the home page.</li>
<li>All submitted images are non-returnable.</li>
<li>Participants are entirely responsible for all entry-related costs.</li>
	<br>			
<p>2. SUBMISSION OF ENTRIES</p>
<li>Images of not more than 10 MB in size must be submitted online and the organiser may request for higher resolution images subsequently.</li>
<li>Images that have won prizes in prior competitions, or have been used for commercial purposes and/or been published will not be eligible.</li>
<li>An image can only be submitted once. Same image cannot be submitted for the same competion as well as in any another competition held by LensQueen.</li>
	<br>			
<p>3. Copyright/Intellectual Property</p>
<li>Each image submitted must be the original and unpublished work of the participant who must also be its copyright owner.</li>
<li>The participant shall retain copyright to the image entered for the competition.</li>
<li>By submitting an image for the competition, the participant will be regarded as having granted the organiser the right to use the image in print, broadcast and/or electronic media without any fee payment, for the purposes of promoting the competition.</li>
<br>
<p>4. PRIZES</p>
<li>Prizes are non-transferrable and non-exchangeable for cash or in kind.</li>
<li>The organiser reserves the right to change or replace the competition prizes and/or modify the rules and regulations of competition as and when necessary, without prior notice.</li>
<li>The organiser shall not be liable for any fault with any of the prizes and any issues or queries relating to the condition of the prizes should be referred to the manufacturer/supplier of the prizes.</li>
<br>
<p>5. JUDGING AND RESULTS</p>

<li>The images will be selected based on the number of votes it receive and will be declared winner of the competition.</li>
<li>There is no extra judge alloted. Purely judge on the number of votes received.</li>
<li>Results of the competition will be posted on the website and winners will also be notified via email.</li>
<br>
<p>6. ACCEPTANCE OF RULES AND REGULATIONS</p>
<li>By submitting an image for the competition, the participant will be regarded as having accepted and agreed to be bound by the rules and loss, theft or destruction of the images.</li>
<br>
<p>7. ORGANISERS</p>
<li>The "LensQueen: Online Photography Competition" is organised by the Union Photography Club, Guwahati.</li>
            </div>
        </div>
    </div>
  </div>


  </div>

</section>
<!--==============================archieve block=================================-->
<!--==============================footer=================================-->
<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_12">
            <a href="#" onClick="goToByScroll('top'); return false;" class="top"></a><br> LensQueen

          <div class="copy"><span class="brand">LensQueen: Online Photography System </span>  &copy; <span id="copyright-year"></span>  | <a href="#"></a>
          </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>
<script type="text/javascript" src="js/jquerypp.custom.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript">

      $( '#carousel' ).elastislide( {
        minItems : 8
      } );

    </script>
</body>
</html>
