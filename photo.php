<?php

	include 'myconnect.php';
  session_start("session2");

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
    $theme_id = $_POST["th_id"];
    if ($theme_id == -1) {
        echo "<script language=javascript>alert('Please select the theme')</script>";
    } else {
		$imagename = $_FILES["myimage"]["name"];
		$desc=mysql_real_escape_string($_POST['desc']);

		$imagetmp = addslashes (file_get_contents($_FILES['myimage']['tmp_name']));

		$sql="insert into photo values('',$theme_id, '$imagename','A','$imagetmp','0','$desc')";
		$s=mysqli_query($conn,$sql);


		if($s)
		{
      $photo_id = mysqli_insert_id($conn);
      $sql = "select ssn from registration where email='".$_SESSION['login_user']."'";
      $result = mysqli_query($conn, $sql);
      $row = mysqli_fetch_assoc($result);
      $sql="insert into upload_user_photo values (".$row['ssn'].",".$photo_id.")";
      $s=mysqli_query($conn,$sql);
      if($s) {
        echo "<script language=javascript>alert('Data has been inserted successfully')</script>";
      } else {
        echo "<script language=javascript>alert('!!!Unable to insert map')</script>";
      }
		}
		else
			echo "<script language=javascript>alert('!!!Unable to insert photo')</script>";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
     <head>
     <title>Lensqueen</title>
     <meta charset="utf-8">
     <meta name="format-detection" content="telephone=no" />
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
    <link href="./assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
     <script src="js/jquery.js"></script>
    <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/script.js"></script>
     <script src="js/superfish.js"></script>
     <script src="js/jquery.equalheights.js"></script>
     <script src="js/jquery.easing.1.3.js"></script>
     <script>

    function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}
	
	function viewGallery(){
		location.href = "public.html";
	}

     </script>


     </head>
     <body class="" id="top">
<!--==============================header=================================-->
<header>
  <div class="container">
    <div class="grid">
            <!--h1><a href="index.html"><img src="images/lens.jpg" alt="LensQueen: Online Photography System" width="400" height="210"></a></h1-->

    </div>
  </div>
  <div class="clear"></div>
  <div class="menu_block">
    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
			<ul class="sf-menu">
       <li class="current"><a href="index.html">Home</a></li>
       <li><a href="2daypick.html">Today's Pick</a></li>
       <li><a href="contestarchive.php">Contest Archive</a></li>
       <li><a href="lensqueentips.php">Lensqueen Tips</a></li>
       <li><a href="exprtcorner.php">Expert Corner</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="aboutus.php">About Us</a></li>
     </ul>
    </nav>
    <div class="clear"></div>
  </div>
</header>

<!--=====================Content======================-->
<!--section class="content"><div class="ic"></div>
  <div class="container_12">
    <div class="grid_4">
      <h4>section 1</h4>
      <img src="images/page2_img1.jpg" alt="" class="img_inner">
      sec 2    </div>
    <div class="grid_4">
      <h4>sec 3</h4>
      <img src="images/page2_img2.jpg" alt="" class="img_inner">
      sec 4
    </div>
    <div class="grid_4">
      <h4>sec 5</h4>
      <img src="images/page2_img3.jpg" alt="" class="img_inner">
      sec 6<br>
    </div>
    <div class="clear"></div>
  </div>

<--/section>
<!--==============================Bot_block=================================-->
<!--=====================archieve block======================-->
<!section class="content"><div class="ic"></div>
  <div class="container_12">

<h1 align="center">Photography Competition 2016</h1>
<h4 align="right"><a href=".\logout.php">Logout</a></h4>
<h6 align="right"><i>contact us: +91-801-101-2832 <i></h6><hr/>
<br/>
<form action = "#" method = "post" enctype="multipart/form-data">
<table border="0" align="center" style="height:300px;300px;">
	<tr>
	<td colspan="2" align="center"><b>Upload Here</b></td>
	</tr>

	<tr>
	<td>Select File to Upload:</td>
	</tr>
	<tr>
	<td><input type="file" name="myimage"  required></td>
	</tr>

	<tr>
	<td>A Brief Description of photo:</td>
	</tr>
	<tr>
	<td> <textarea rows="5" cols="35" name="desc" placeholder="Description" required> </textarea></td>
	</tr>
  <tr><td><input id="theme_id" name="th_id" type="hidden" value="-1" /></td></tr>
  <tr>
  <td class="dropdown">
      <button class="btn btn-primary" id="selectButton" data-toggle="dropdown">
        Select Theme
        <span class="caret"></span>      </button>
      <ul class="dropdown-menu scrollable-menu" id="themes">
        <script>
          $.ajax({
          type: "GET",
            url: './get_themes.php',
            dataType: 'json',
            success: function(json) {
              console.log(json);
              if (json) {
                for (var i in json) {
                  var title = json[i].title;
                  var id = json[i].id;
                  $("#themes").append(`<li><a href="#" data-pdsa-dropdown-val="${id}">${title}</a></li>`);
                }
                $("#themes li a").on("click", function () {
                    // Get text from anchor tag
                    var id = $(this).data('pdsa-dropdown-val');
                    $("#theme_id").val(id);
                    // Add text and caret to the Select button
                    var text = $(this).text();
                    $("#selectButton").html(text + '&nbsp;<span class="caret"></span>');
                  });
              }
            }
          });
        </script>
      </ul>  </td>
  </tr>

	<tr>
	<td ><input type="submit" name="submit"  value="Upload" >
	  <input type="submit" name="submit2"  value="View Gallery" onClick="viewGallery()"></td>
	<td><input type="reset" name="reset"  value="Cancel"></td>
	</tr>
</table>
</form>



  </div>

</section>
<!--==============================archieve block=================================-->
<!--==============================footer=================================-->
<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_12">
            <a href="#" onClick="goToByScroll('top'); return false;" class="top"></a><br> LensQueen

          <div class="copy"><span class="brand">LensQueen: Online Photography System </span>  &copy; <span id="copyright-year"></span>  | <a href="#"></a>
          </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>
<script type="text/javascript" src="js/jquerypp.custom.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript">

      $( '#carousel' ).elastislide( {
        minItems : 8
      } );

    </script>
</body>
</html>
