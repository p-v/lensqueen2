<!DOCTYPE html>
<html lang="en">
     <head>
     <title>Lensqueen</title>
     <meta charset="utf-8">
     <meta name="format-detection" content="telephone=no" />
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
     <script src="js/jquery.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/script.js"></script>
     <script src="js/superfish.js"></script>
     <script src="js/jquery.equalheights.js"></script>
     <script src="js/jquery.easing.1.3.js"></script>
     <script>

    function goToByScroll(id){$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');}

     </script>


     </head>
     <body class="" id="top">
<!--==============================header=================================-->
<header>
  <div class="container">
    <div class="grid">
            <h1><a href="index.html"><img src="images/lens.jpg" alt="LensQueen: Online Photography System" width="200" height="150"></a></h1>

    </div>
  </div>
  <div class="clear"></div>
  <div class="menu_block">
    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
      <ul class="sf-menu">
       <li class="current"><a href="index.html">Home</a></li>
       <li><a href="2daypick.html">Today's Pick</a></li>
       <li><a href="contestarchive.php">Contest Archive</a></li>
       <li><a href="lensqueentips.php">Lensqueen Tips</a></li>
       <li><a href="exprtcorner.php">Expert Corner</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="aboutus.php">About Us</a></li>
     </ul>
    </nav>
    <div class="clear"></div>
  </div>
</header>

<!--=====================Content======================-->
<!--section class="content"><div class="ic"></div>
  <div class="container_12">
    <div class="grid_4">
      <h4>section 1</h4>
      <img src="images/page2_img1.jpg" alt="" class="img_inner">
      sec 2    </div>
    <div class="grid_4">
      <h4>sec 3</h4>
      <img src="images/page2_img2.jpg" alt="" class="img_inner">
      sec 4
    </div>
    <div class="grid_4">
      <h4>sec 5</h4>
      <img src="images/page2_img3.jpg" alt="" class="img_inner">
      sec 6<br>
    </div>
    <div class="clear"></div>
  </div>

<--/section>
<!--==============================Bot_block=================================-->
<!--=====================archieve block======================-->
<!section class="content"><div class="ic"></div>
  <div class="container_12">


  <?php

	include 'myconnect.php';

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$name=$_POST['name'];
		$midname=$_POST['midname'];
		$lname=$_POST['lname'];
		$dob=$_POST['dob'];
		$email=$_POST['email'];
		$password=$_POST['password'];
		$address1=$_POST['address1'];
		$address2=$_POST['address2'];
		$city= $_POST['city'];
		$phone= $_POST['phone'];
		$pin_no= $_POST['pin_no'];
		$state= $_POST['state'];
		$experience= $_POST['exp'];
		
		$password = hash('sha256',$password);

		$mydate = '1998/01/01';
		if(!filter_var($email,FILTER_VALIDATE_EMAIL)==true){
		echo("Error");
		}else{
		$q = "SELECT email FROM registration WHERE email='$email'";
		$cq = mysqli_query($conn,$q);
		$ret = mysqli_num_rows($cq);
		if($ret == true){
			echo "<script language=javascript>alert('The email you have entered already exist')</script>";
			}

			else if($dob > $mydate)
			{
				echo "<script language=javascript>alert('Invalid Date of Birth.!!')</script>";
			}

			elseif(strlen($phone)<10 || strlen($phone)>10 )
			{
				echo "<script language=javascript>alert('Enter Valid Phone no!!')</script>";
			}


			elseif(strlen($pin_no)<7 || strlen($pin_no)>7 )
			{
				echo "<script language=javascript>alert('Enter alid Pin Number!!')</script>";
			}

			else
			 {
				$sql='insert into registration values("","'.$name.'","'.$midname.'","'.$lname.'","'.$dob.'","'.$email.'","'.$password.'","'.$address1.'","'.$address2.'","'.$city.'","'.$phone.'","'.$pin_no.'","'.$state.'","'.$experience.'")';
				$s=mysqli_query($conn,$sql);

				if($s)
				{
					echo "<script language=javascript>alert('Registration Successful')</script>";
				}
				else
					echo "<script language=javascript>alert('Registration Failed')</script>";
			}
		}
	}
?>
  
<form action = "#" method = "post">
<table border="0" align="center" style="height:300px;300px;">
	<tr>
	<td colspan="2" align="center"><h2>Registration</h2></td>
	</tr>

	<tr>
	<td>Name :</td><td><input type="text" size="40" style="30" name="name" placeholder="Enter the Name" required pattern="^[A-Za-z]+"/>(Alphabets only)</td>
	</tr>
	<br>

	<tr>
		<td>Middle Name :</td>
		<td><input type="text" size="40" style="30" name="midname" placeholder="Enter the middle Name" pattern="^[A-Za-z]+"/></td>
	</tr>
<br>
	<tr>
	<td>Last Name :</td>
	<td><input type="text" size="40" style="30" name="lname" placeholder="Enter the Last Last Name" required pattern="^[A-Za-z]+"/></td>
	</tr>
<br>

	<tr>
	<td>Date of Birth:</td>
	<td><input type="date" name="dob"  required/>(Only for adults i.e, above 18 years)</td>
	</tr>


	<tr>
	<td>Email:</td>
	<td><input type="email" name="email" placeholder="Enter the email Address" required/></td>
	</tr>


	<tr>
	<td>Password:</td>
	<td><input type="password" name="password" placeholder="Enter the Pasword" required/></td>
	</tr>


	<tr>
	<td>Address 1:</td>
	<td> <textarea rows="5" cols="35" name="address1" placeholder="Enter the Address1" required> </textarea></td>
	</tr>

	<tr>
	<td>Address 2:</td>
	<td><textarea rows="5" cols="35" name="address2" placeholder="Enter the Address2"> </textarea></td>
	</tr>

	<tr>
	<td>City:</td>
	<td><input type="tel" size="40" style="30" name="city" placeholder="Enter the City" required/></td>
	</tr>

	<tr>
	<td>Phone:</td>
	<td><input type="number" name="phone" placeholder="Enter the phone Number" pattern="[0-1]{10}" onChange="validatephone(this);" required/>(exactly 10 characters)</td>
	</tr>

	<tr>
	<td>Pin No:</td>
	<td><input type="number" size="30" style="30" name="pin_no" placeholder="Enter the Pin No" required/></td>
	</tr>

	<tr>
	<td>State:</td>
	<td><input type="tel" size="40" style="30" name="state" placeholder="Enter the State" required pattern="^[A-Za-z]+"/></td>
	</tr>
	<tr>
	<td>Experience:</td>
	<td><textarea rows="5" cols="35" name="exp" placeholder="Share your experience" required> </textarea></td>
	</tr>

	<tr>
	<td ><input type="submit" name="submit"  value="Register" ></td>
	<td> </td>
	<td><input type="reset" name="reset"  value="Cancel"></td>
	<tr>
	<td>
	<td><a href="login.php">Login Now</td>
  </td>
	</tr>
	</tr>
</table>
</form><br/><br/><br/><br/><br/><br/><br/><br/><br/>


  </div>

</section>
<!--==============================archieve block=================================-->
<!--==============================footer=================================-->
<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_12">
            <a href="#" onClick="goToByScroll('top'); return false;" class="top"></a><br> LensQueen

          <div class="copy"><span class="brand">LensQueen: Online Photography System </span>  &copy; <span id="copyright-year"></span>  | <a href="#"></a>
          </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</footer>
<script type="text/javascript" src="js/jquerypp.custom.js"></script>
    <script type="text/javascript" src="js/jquery.elastislide.js"></script>
    <script type="text/javascript">

      $( '#carousel' ).elastislide( {
        minItems : 8
      } );

    </script>
</body>
</html>
